from dataclasses import dataclass


@dataclass(frozen=True)
class Cities:
    odesa = "odesa,ua"
    kyiv = "kyiv,ua"
    kharkiv = "kharkiv,ua"
    dnipro = "dnipro,ua"
    donetsk = "donetsk,ua"
    lviv = "lviv,ua"
    zhytomyr = "zhytomyr,ua"
