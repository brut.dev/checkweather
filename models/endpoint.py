from dataclasses import dataclass


@dataclass(frozen=True)
class CurrentWeather:
    q: str = "q"
    appid: str = "appid"
    lang: str = "lang"
