from dataclasses import dataclass


@dataclass(frozen=True)
class Lang:
    en = "en"
    ua = "ua"
    ru = "ru"
