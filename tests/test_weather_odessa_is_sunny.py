import pytest
import consts
import allure
from helpers.file_helper import FileHelper, get_api_key_from_file


@allure.feature("Get API key")
@allure.story("Get API key from openweathermap")
@pytest.mark.dependency(name="apikey")
def test_get_apikey_from_openweather(ui):
    with allure.step("Authorization in openweathermap"):
        ui.steps.auth.get_authorized(consts.EMAIL, consts.PASSWORD)
        assert ui.steps.auth.is_authorized(), "Seems like not authorized"
    with allure.step("Go to page with API key and get it"):
        api_key = ui.steps.home_page.api_key.get_api_keys()
        assert api_key is not None, "api_key is None"
    with allure.step("Write API key in file"):
        FileHelper().write_file(api_key.get(consts.DEFAULT_API_KEY_NAME), consts.API_KEY_FNAME)
        assert FileHelper().is_file(consts.API_KEY_FNAME), "File not found!"
        assert get_api_key_from_file is not None


@allure.feature("Check weather")
@allure.story("Check weather in Odessa, the weather should be sunny")
@pytest.mark.dependency(name="check", depends=["apikey"])
def test_check_weather_in_odessa(api):
    with allure.step("Call to endpoint current weather"):
        current_weather = api.current_weather.get_current_weather_in_odessa()
        assert current_weather.status_code == 200, current_weather.status_code
    with allure.step("Check weather"):
        assert api.current_weather.is_weather(current_weather)
    with allure.step("Weather in Odessa is sunny"):
        api.current_weather.is_sunny(current_weather)
