from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager
from webdriver_manager.opera import OperaDriverManager
from webdriver_manager.firefox import GeckoDriverManager
from webdriver_manager.microsoft import EdgeChromiumDriverManager
from helpers.file_helper import get_api_key_from_file
from consts import DEFAULT_URL
from ui.ui_facade import UIFacade
from api.steps.api_facade import ApiFacade
import pytest


def pytest_addoption(parser):
    parser.addoption('--b', action='store', default="None",
                     help="Choose browser for executing. (for example: firefox, opera, edge), Chrome by default")


@pytest.fixture(scope='session')
def driver(request):
    browser = request.config.getoption("--b")

    if browser.lower() == "firefox":
        driver = webdriver.Firefox(executable_path=GeckoDriverManager().install())
    elif browser.lower() == "opera":
        driver = webdriver.Opera(executable_path=OperaDriverManager().install())
    elif browser.lower() == "edge":
        driver = webdriver.Edge(executable_path=EdgeChromiumDriverManager().install())
    else:
        driver = webdriver.Chrome(executable_path=ChromeDriverManager().install())

    driver.maximize_window()
    driver.get(DEFAULT_URL)
    yield driver
    driver.quit()


@pytest.fixture(scope='session')
def ui(driver):
    yield UIFacade(driver)


@pytest.fixture(scope='session')
def api():
    yield ApiFacade(get_api_key_from_file())
