from ui.pages.componets.header import Header
from ui.pages.auth_form import AuthForm
from ui.pages.api_keys import ApiKeys
from ui.pages.componets.home_page_tabs import HomePageTabs


class PagesFacade:
    def __init__(self, driver):
        self.header = Header(driver)
        self.home_page_tabs = HomePageTabs(driver)
        self.auth_form = AuthForm(driver)
        self.api_keys = ApiKeys(driver)
