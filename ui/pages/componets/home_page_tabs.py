from ui.wrapper.core import BasePage, Element
from selenium.webdriver.common.by import By
from helpers.retry import retry


class HomePageTabs(BasePage):
    api_keys_locator = (By.XPATH, "//a[@href='/api_keys']")
    new_products_locator = (By.XPATH, "//a[@href='/']")
    services_locator = (By.XPATH, "//a[@href='/myservices']")
    my_profile_locator = (By.XPATH, "//a[@href='/home']")
    alert_info_locator = (By.CSS_SELECTOR, ".alert-info")
    api_key_table_locator = (By.CSS_SELECTOR, ".material_table > tbody")

    def __init__(self, driver):
        super().__init__(driver)
        self.api_keys = Element(self.driver, self.api_keys_locator)
        self.alert_info = Element(self.driver, self.alert_info_locator)
        self.api_key_table = Element(By.CSS_SELECTOR, ".material_table > tbody")

    def get_alert_info_text(self):
        return self.alert_info.text

    @retry(timeout=1, tries=2)
    def api_keys_button_click(self):
        self.api_keys.wait_clickable().click()

