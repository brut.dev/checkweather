from ui.wrapper.core import BasePage, Element
from selenium.webdriver.common.by import By


class Header(BasePage):
    sign_in_locator = (By.XPATH, "//li[@id='sign_in']/a")
    user_name_locator = (By.XPATH, "//span[@id='open-dropdown']")

    def __init__(self, driver):
        super().__init__(driver)
        self.sign_in = Element(self.driver, self.sign_in_locator)
        self.user_name = Element(self.driver, self.user_name_locator)

    def sign_in_button_click(self):
        self.sign_in.click()

    def user_name_get_text(self):
        return self.user_name.text

    def user_name_is_displayed(self):
        return self.user_name.is_displayed()
