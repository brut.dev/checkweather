from ui.wrapper.core import BasePage, Element
from selenium.webdriver.common.by import By


class ApiKeys(BasePage):
    name_key_input_locator = (By.ID, "api_key_form_name")
    generate_key_button_locator = (By.XPATH, "//input[@value='Generate']")
    first_key_locator = (By.CSS_SELECTOR, "table.api-keys pre")
    api_key_table_locator = (By.CSS_SELECTOR, ".material_table > tbody")

    def __init__(self, driver):
        super().__init__(driver)
        self.name_key_input = Element(self.driver, self.name_key_input_locator)
        self.generate_key_button = Element(self.driver, self.generate_key_button_locator)
        self.first_key = Element(self.driver, self.first_key_locator)
        self.api_key_table = Element(self.driver, self.api_key_table_locator)

    def first_key_get_text(self):
        return self.first_key.text

    def generate_key_button_click(self):
        self.generate_key_button.click()

    def name_key_input(self, text):
        self.name_key_input.send_keys(text)

    def delete_key_alert_accept(self):
        alert = self.driver.switch_to.alert
        assert "Do you want to remove this key?" in alert.text
        alert.accept()

    def delete_key_alert_cancel(self):
        alert = self.driver.switch_to.alert
        assert "Do you want to remove this key?" in alert.text
        alert.dismiss()

    '''    #!   parse table   !#      '''

    def api_key_table_rows(self, index):
        return self.api_key_table.find_elements((By.CSS_SELECTOR, f"tr > td:nth-child({index})"))

    def api_key_table_name_row(self):
        return self.api_key_table_rows("2")

    def api_key_table_key_row(self):
        return self.api_key_table_rows("1")

    def get_api_keys(self):
        return {key.text: value.text for (key, value) in
                dict(zip(self.api_key_table_name_row(), self.api_key_table_key_row())).items()}


