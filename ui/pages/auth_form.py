from ui.wrapper.core import BasePage, Element
from selenium.webdriver.common.by import By


class AuthForm(BasePage):
    email_input_locator = (By.ID, "user_email")
    password_input_locator = (By.ID, "user_password")
    submit_button_locator = (By.XPATH, "//input[@value='Submit']")
    remember_me_checkbox_locator = (By.CSS_SELECTOR, "#user_remember_me")
    notice_panel_locator = (By.CSS_SELECTOR, ".panel > .panel-body")

    def __init__(self, driver):
        super().__init__(driver)
        self.email_input = Element(self.driver, self.email_input_locator)
        self.password_input = Element(self.driver, self.password_input_locator)
        self.submit_button = Element(self.driver, self.submit_button_locator)
        self.notice_panel = Element(self.driver, self.notice_panel_locator)

    def email_input_send(self, text):
        self.email_input.send_keys(text)

    def password_input_send(self, text):
        self.password_input.send_keys(text)

    def submit_button_click(self):
        self.submit_button.click()

    def is_notice_panel_displayed(self):
        return self.notice_panel.is_displayed()

    def get_notice_text(self):
        return self.notice_panel.text
