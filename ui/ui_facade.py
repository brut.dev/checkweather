from ui.steps.steps_facade import UIStepsFacade
from ui.pages.pages import PagesFacade


class UIFacade:

    def __init__(self, driver):
        self.pages = PagesFacade(driver)
        self.steps = UIStepsFacade(self.pages)
