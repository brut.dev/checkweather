from selenium.webdriver.remote.webelement import WebElement
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait
import consts
from selenium.common.exceptions import NoSuchElementException, StaleElementReferenceException, \
    ElementNotVisibleException


class BasePage:
    def __init__(self, driver):
        self.driver = driver

    def go_to(self, url):
        return self.driver.get(url)

    def back(self):
        return self.driver.back()

    def reload_page(self):
        self.driver.refresh()


class Element:
    def __init__(self, driver, locator):
        self.driver = driver
        self.locator = locator

    @property
    def text(self):
        return self.find_element().text

    def find_element(self, timeout=consts.TIME_OUT):
        return self.wait_elem(timeout=timeout).until(EC.presence_of_element_located(self.locator),
                                                     message=f"Can't find element by locator {self.locator}")

    def find_elements(self, locator) -> WebElement:
        return self.find_element().find_elements(*locator[:2])

    def click(self, wait_clickable=True):
        if wait_clickable:
            self.wait_clickable()
        self.find_element().click()

    def wait_clickable(self, timeout=consts.TIME_OUT):
        element = self.wait_elem(timeout).until(EC.element_to_be_clickable(self.locator), message=self.locator)
        return element

    def send_keys(self, text):
        self.wait_clickable()
        element = self.find_element()
        element.clear()
        element.send_keys(text)

    def wait_elem(self, timeout=consts.TIME_OUT):
        return WebDriverWait(driver=self.driver, timeout=timeout, ignored_exceptions=(
            NoSuchElementException, StaleElementReferenceException, ElementNotVisibleException))

    def is_displayed(self, timeout=consts.TIME_OUT):
        return self.wait_elem(timeout).until(EC.presence_of_element_located(self.locator), message=self.locator)
