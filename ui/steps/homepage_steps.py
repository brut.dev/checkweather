from ui.steps.base_steps import BaseSteps


class HomePageStepsFacade:

    def __init__(self, pages):
        self.api_key = ApiKeySteps(pages)


class ApiKeySteps(BaseSteps):

    def open_api_keys_tab(self):
        self.pages.home_page_tabs.api_keys_button_click()

    def get_api_keys(self):
        self.open_api_keys_tab()
        return self.pages.api_keys.get_api_keys()
