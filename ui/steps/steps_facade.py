from ui.steps.homepage_steps import HomePageStepsFacade
from ui.steps.auth_steps import AuthSteps


class UIStepsFacade:

    def __init__(self, pages):
        self.auth = AuthSteps(pages)
        self.home_page = HomePageStepsFacade(pages)

