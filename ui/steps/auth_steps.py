from ui.steps.base_steps import BaseSteps


class AuthSteps(BaseSteps):

    def is_authorized(self):
        return self.pages.header.user_name_is_displayed()

    def get_authorized(self, email, password):
        self.pages.header.sign_in_button_click()
        self.pages.auth_form.email_input_send(email)
        self.pages.auth_form.password_input_send(password)
        self.pages.auth_form.submit_button_click()

