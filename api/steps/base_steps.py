from api.endpoints import WeatherEndpoints


class BaseWeatherSteps:
    def __init__(self, api_key: str):
        self._api_key = api_key
        self.endpoint = WeatherEndpoints()
