from api.steps.current_weather import CurrentWeather
from api.steps.forecast_four_day import FourDayWeather


class ApiFacade:
    def __init__(self, api_key):
        self.current_weather = CurrentWeather(api_key)
        self.forecast_four_day = FourDayWeather(api_key)



