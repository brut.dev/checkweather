from models.cities import Cities
from api.steps.base_steps import BaseWeatherSteps


class FourDayWeather(BaseWeatherSteps):
    def __init__(self, api_key):
        super().__init__(api_key)

    def get_forecast_four_day_in_odessa(self):
        return self.endpoint.get_forecast_four_day(city=Cities.odesa, api_key=self._api_key)

    def get_forecast_four_day_in_kyiv(self):
        return self.endpoint.get_forecast_four_day(city=Cities.kyiv, api_key=self._api_key)

    def get_forecast_four_day_in_lviv(self):
        return self.endpoint.get_forecast_four_day(city=Cities.lviv, api_key=self._api_key)
