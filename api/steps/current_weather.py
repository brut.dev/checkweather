from models.cities import Cities
from api.steps.base_steps import BaseWeatherSteps


class CurrentWeather(BaseWeatherSteps):
    def __init__(self, api_key):
        super().__init__(api_key)
        self._api_key = api_key

    def get_current_weather_in_odessa(self):
        return self.endpoint.get_current_weather(city=Cities.odesa, api_key=self._api_key)

    def get_current_weather_in_kyiv(self):
        return self.endpoint.get_current_weather(city=Cities.kyiv, api_key=self._api_key)

    def get_current_weather_in_lviv(self):
        return self.endpoint.get_current_weather(city=Cities.lviv, api_key=self._api_key)

    def is_weather(self, request):
        return request.response.get('weather', False)

    def is_sunny(self, request):
        description = request.response.get('weather')[0].get('description')
        assert description == 'sunny', description
