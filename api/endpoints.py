from consts import API_DEFAULT_URL
from models.endpoint import CurrentWeather
from api.httpclient import request


class BaseEndpoint:
    def __init__(self):
        self.api = request


class WeatherEndpoints(BaseEndpoint):
    def __init__(self):
        super().__init__()
        self.current_weather_url = "/data/2.5/weather"
        self.forecast_four_day = "/data/2.5/forecast/hourly"

    def get_current_weather(self, city, api_key):
        return self.api("GET", f"{API_DEFAULT_URL}{self.current_weather_url}",
                        params={CurrentWeather.q: city, CurrentWeather.appid: api_key})

    def get_forecast_four_day(self, city, api_key):
        return self.api("GET", f"{API_DEFAULT_URL}{self.current_weather_url}",
                        params={CurrentWeather.q: city, CurrentWeather.appid: api_key})



