import requests
import json
import allure
from helpers.retry import retry
from api.http_exception import HttpException


@retry(timeout=2, tries=3)
def request(method, url, data=None, headers=None, json=None, params=None, **kwargs):
    request_args = {
        "url": url,
        "method": method,
        "headers": headers,
        "params": params,
        "data": data,
        "json": json
    }
    request_args.update(**kwargs)
    with allure.step(f"Method:{method} request to: {url}"):
        response = requests.request(**request_args)

    if response.status_code >= 400:
        raise HttpException(request_args.get("url"), response.status_code, response.text,
                            response.reason)

    return ResponseModel(response)


class ResponseModel:
    def __init__(self, resp_obj):
        self.resp_obj = resp_obj
        self.response = self._get_response()
        self.status_code = resp_obj.status_code

    def _get_response(self):
        try:
            return self.resp_obj.json()
        except json.decoder.JSONDecodeError:
            return self.resp_obj.text


if __name__ == "__main__":
    response = request("GET", "http://google.com/qqqq")
