""" example custom exception """


class HttpException(Exception):
    def __init__(self, *kwargs):
        print('Http exception:', *kwargs)
