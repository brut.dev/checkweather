import os
from config import ROOT_DIR, TEMP_DIR
from consts import API_KEY_FNAME


class FileHelper:

    def __init__(self):
        self.root_dir = ROOT_DIR
        self.temp_dir = TEMP_DIR

    def write_file(self, data, filename):
        if not os.path.exists(self.temp_dir):
            os.makedirs(self.temp_dir)
        with open(os.path.join(self.temp_dir, filename), "w") as writer:
            writer.write(data)

    def read_file(self, filename):
        try:
            with open(os.path.join(self.temp_dir, filename), "r") as reader:
                data = reader.readlines()
        except FileNotFoundError:
            print(f"File not found: {filename}")
        return data

    def is_file(self, filename):
        return os.path.isfile(os.path.join(self.temp_dir, filename))


def get_api_key_from_file():
    try:
        return get_api_key_from_file.apikey
    except AttributeError:
        get_api_key_from_file.api_key = "".join(FileHelper().read_file(API_KEY_FNAME))
    return get_api_key_from_file.api_key
