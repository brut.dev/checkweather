### DESCRIPTION
    
    This is test task. Check weather in Odessa 

##### PROJECT STRUCTURE

    api: contains endpoints and steps
    ui: contains selenium elements and steps
    models: contains dataclasses with parameters 
    helplers: different helplers
    tests: contains tests
    allure: folder for allure report

##### TESTS RUN

    py.test -v --alluredir=./allure

##### Test scenarios:
    1.  Log in to your account
        Get api key from your account via UI
        Save api key to the file.
    2.  Get api key from the file
        Send request to get forecast for Odessa
        Check api call was successful, check whether weather in Odessa is sunny 
        (hint: look for “description” parameter in response)

##### Technologies for use:
    Chromedriver, Selenium, Python, Pytest, Allure.
   

